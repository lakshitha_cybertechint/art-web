@extends('layouts.front.master')
@section('pageTitle','Arists')


<style type="text/css">

.util-bar.semi-compact {
    width: 120%!important;
}

.search-bar form{
   max-width: 960px!important;
}
</style>

<!-- CSS FOR THIS PAGE -->
@section('css')

@stop


<!-- BODY -->

@section('content')

      <!-- All arts data -->

      <div id="top" class="page theme-c  cbg_white" aria-hidden="false">

            <div class="page-body has-footer">

            <div class="page-header bg-theme has-image">
              <div data-role="lazy-image" class="image-lazy corner-top-left width-full height-full loaded" data-lazy-image-scale="true" data-lazy-image-animate="true" alt="" aria-hidden="true" data-lazy-image-url="//lh4.ggpht.com/aObw6_H5guaPIDoD15rjlYB7rHzDVoiSUzBsu12t9UsiZR7C4nXz78A0xLvUOdVat9UYdY6B3hJBJynVIdAnD3KSJwhQ96DCM2LVQR0DcQ" data-lazy-image-width="4000" data-lazy-image-height="1269" style="background-image: url(&quot;//lh4.ggpht.com/aObw6_H5guaPIDoD15rjlYB7rHzDVoiSUzBsu12t9UsiZR7C4nXz78A0xLvUOdVat9UYdY6B3hJBJynVIdAnD3KSJwhQ96DCM2LVQR0DcQ=s1600&quot;);"></div>
              <header class="page-unit">
                @if ($query == "")
                  <h1 class="hero-title">Artists</h1>
                @else
                  <h1 class="hero-title">Artists of {{$query}}</h1>
                @endif

                <nav class="search-bar util-bar semi-compact">
                  <form class="item" method="GET" action="{{url('artists/search')}}">
                    <input class="search-bar-query light rounded-left text-ellipsis" name="q" value="{{$query}}" placeholder="Search for an Artist" required type="search">
                    <button class="search-bar-clear button icon-clear-input icon-only text-neutral text-dark-hover" type="button" data-role="clear-input"><span class="visually-hidden">Clear search</span></button>
                    <button class="search-bar-submit button dark-hover  icon-search rounded-right" type="submit" formnovalidate=""><span class="">Search</span></button>
                  </form>
                </nav>
              </header>
            </div>


<!-- Cutomized Filter panel -->
        <a id="filter-toggle" class="menu-button corner-middle-left z-extra-high button square huge bold  c-blue dark-hover icon-after icon-arrow-right" aria-controls="filter" aria-expanded="false" tabindex="0">
          <span class="menu-button-text ss-hidden">Filter</span>
        </a>


        <nav class="sidebar" aria-hidden="true" tabindex="-1">


        <div class="main-navigation">
        <ul class="list-links list-plain">


        </ul>


      </div>


    </nav>

    <!-- End Cutomized Filter panel -->


          <div data-role="facets-and-results">
          <div class="uhm" data-role="search-results-container">

            <div data-role="search-page " class="search-page">
              <div class="filters" data-role="filters" id="filters" aria-labelledby="filter-result-text" aria-hidden="false">
            <a href="#" class="open-filters button huge square icon-before icon-arrow-up" data-role="open-filters" aria-controls="filters" aria-expanded="false" tab-index="0" data-tab-inverted="" tabindex="-1"><span class="visually-hidden">Open </span>filters</a>
            <header class="filters-header">
              <div class="result-count text-bold" id="filter-result-text">
                  <p>3720 results found</p>

              </div>
              <button class="button square huge icon-arrow-down" data-role="close-filters" tabindex="0"><span class="visually-hidden">Close </span><span class="label-close-filters">filters</span></button>
            </header>
            <div id="filter" class="filter-container" data-role="filter-container">
              <ul class="search-filters list-plain">
                    <li class="open">
                      <header>
                        <h5>Artists ({{count($artists)}})</h5>
                      </header>
                      <section class="filter-body">
                          <div class="artist-suggest" data-role="artist-suggest">
                            <div class="artist-suggest-input-wrapper">
                              <input data-role="artist-suggest-input" class="facet-search ui-autocomplete-input" id="artist-suggest-input" tabindex="0" placeholder="Find artist" autocomplete="off" type="search">
                              <button class="search-bar-clear button icon-clear-input icon-only text-neutral text-dark-hover" type="button" data-role="clear-search-filter-input" tabindex="0"><span class="visually-hidden">Clear find artist</span></button>
                            </div>

                            <div class="artist-suggest-list search-facets list-plain" id="artist-suggest-output"><ul class="ui-autocomplete ui-front ui-menu ui-widget ui-widget-content" id="ui-id-2" tabindex="0" style="display: none;"></ul></div>
                          </div>

                        <ul class="search-facets list-plain hasShowMore " aria-hidden="false" data-role="artist-filter">
                          @foreach ($artists as $fArtist)
                            <li class="has-results"><input data-role="apply-filter" data-url="{{url("/profile/".$fArtist->id)}}" value="{{$fArtist->name}}" id="f_artists_{{$fArtist->id}}" tabindex="0" type="checkbox"><label for="f_artists_{{$fArtist->id}}">{{$fArtist->name}} <span>({{$fArtist->artistArts->count()}})</span> </label></li>
                          @endforeach


                        </ul>
                        <div class="show-more-wrapper">
                            <button class="transparent text-theme text-bold text-uppercase icon-before icon-plus" data-role="show-more" data-loading-text="Loading results..." tabindex="0">Show more</button>
                          </div>
                          <div class="show-less-wrapper">
                            <button class="transparent text-theme text-bold text-uppercase icon-before icon-minus" data-role="show-less" data-loading-text="Loading results..." tabindex="0">Show Less</button>
                          </div>
                      </section>
                    </li>

              </ul>
            </div>
          </div>

          <!-- <script>
                      window.facetJson = [];

          </script> -->

              <div class="search-results-wrapper" data-role="search-results-wrapper">

                <div class="filters-bar">
                  <div class="open-filters-buttton-wrapper">
                    <a id="filter-toggle_m" href="#" class="open-filters button huge square icon-before icon-arrow-right" data-role="open-filters" aria-controls="filters" aria-expanded="false"><span class="visually-hidden">Open </span>filters</a>
                  </div>
                </div>
                <div class="page-unit">

                    <h2 class="item h-overview" role="status" aria-live="polite">
                      @if ($query == "")
                        Arists collection
                      @else
                        {{$artists->total()}} results in the collection
                      @endif
                    </h2>

                    <div id="searchresults" class="searchresults" data-role="searchresults" data-querystring="q=" data-page-size="21">


                        <section class="cols cols-4up">
                          @if ($artists)

                            @foreach ($artists->sortByDesc('created_at') as $artist)
                              <article class="col">
                                <a class="link-teaser" href="{{url('profile/'.$artist->id)}}">
                                  <div class="image-large has-ribbon">

                                    <img  src="{{$artist->image == "" ? url('assets/front/img/default_artist.png') : url($artist->image)}}" class="image-lazy loaded" style="">


                                    <span class="ribbon">{{$artist->name}}</span>
                                  </div>
                                  <p style="text-align: justify; font-size: 13px;">{{substr($artist->tagline, 0, 45)}}...<span class="text-theme icon-arrow-right"></span></p>
                                </a>
                              </article>
                            @endforeach
                          @endif


                        </section>

                        <div class="text-center">
                      @if(isset($query) && count($artists) > 0)
                      {!! $artists->appends(['q' => $query])->render() !!}
                      @elseif (count($artists) > 0)
                        {!! $artists->render() !!}
                      @endif

                        </div>
                  </div>
                </div>
              </div>
              <div class="search-results-overlay" data-role="close-filters"></div>
            </div>
          </div>
          </div>

<!-- End All Arts Details -->

          </div>
    </div>

<script>
  $("#filter").hide(); //hide when loading
  $(document).ready(function(){
      $("#filter-toggle").click(function(){
          $("#filter").slideToggle();

      });
      $("#filter-toggle_m").click(function(){
          $("#filter").show();

      });
  });
</script>

@stop
<!-- JS FOR THIS PAGE -->

@section('js')

@stop
