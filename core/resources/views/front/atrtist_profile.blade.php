@extends('layouts.front.master')
@section('pageTitle','Profile')
<!-- CSS FOR THIS PAGE -->
@section('css')
<style type="text/css">
  p.sub-header {
    padding-bottom: 10px;
    width: 95%;
    font-size: 0.88rem;
    font-weight: 400;
  }
</style>
<style media="screen">
    .artist-name{

    }
  @media (max-width : 800px) {
    .artist-name{
      display: none !important;
    }
  }
</style>
@stop
<!-- BODY -->
@section('content')
<!-- Latest arts data -->
<div id="top" class="page theme-c  cbg_white" aria-hidden="false">
    <div class="page-body has-footer">
        <div class="page-header bg-theme has-image">
            <div data-role="lazy-image" class="image-lazy corner-top-left width-full height-full loaded" data-lazy-image-scale="true" data-lazy-image-animate="true" alt="" aria-hidden="true" data-lazy-image-url="//lh5.ggpht.com/5QuxTHmKR1srOJS_yQwyGqEfcc8AA30P61uYuaU39tMA2KW-eM1aXMjTLTAZp-8ztW0z-q5aIUde2vo8SSKlAdDVHE8VMZUJ-JXKtRNn0BE" data-lazy-image-width="4000" data-lazy-image-height="1269" style="background-image: url(&quot;//lh5.ggpht.com/5QuxTHmKR1srOJS_yQwyGqEfcc8AA30P61uYuaU39tMA2KW-eM1aXMjTLTAZp-8ztW0z-q5aIUde2vo8SSKlAdDVHE8VMZUJ-JXKtRNn0BE=s1600&quot;);"></div>
            <header class="page-unit">
                <h1 class="hero-title artist-name" style="display: inline-block; position: relative; margin: 0 0 0 80px;">{{substr($artist->name, 0, 50)}}</h1>
            </header>
        </div>
        <div class="profile-avatar">
            <img class="avatar" style="display: inline-block;" src="{{$artist->image == "" ? url('assets/front/img/default_artist.png') : url($artist->image)}}">
        </div>
        <div class="bg-theme profile-details" style="height: auto!important; padding: 0 0 15px 0;">
            <div class="details cols cols-text">
                <p class="sub-header"><?php echo nl2br($artist->tagline); ?></p>
                <p class="sub-header">Date of birth: {{$artist->birth_date}}</p>
                <p class="sub-header">No of Arts: {{count($artist->artistArts)}}</p>
                <p class="sub-header" style="text-align: justify;">Descrption: <?php echo  $artist->description; ?></p>
            </div>
        </div>
        <div class="page-unit width-full width-none">
            <section class="cols cols-text">
                <p class="text-intro text-theme">Reacently Added Arts</p>
            </section>
            <ul class="cols cols-4up list-plain">
                @foreach ($artist->artistArts->sortByDesc('created_at') as $aArt)
                <li class="col lazy-content-loaded" tabindex="-1" data-no-tab="">
                    <a href="#" id="s0032V1962" class="anchor" tabindex="-1" aria-hidden="true" data-no-tab="">s0032V1962</a>
                    <a class="link-teaser triggers-slideshow-effect" href="{{url('art/'.$aArt->arts->id)}}" tabindex="0">
                        <div  class="image-lazy image-large  loaded" style="background-image: url({{url($aArt->arts->image)}}); background-position: center;background-size: cover;"></div>
                        <h3 style="text-align: justify; font-size: 15px;" class="text-base text-dark">{{substr($aArt->arts->name, 0, 45)}}</h3>
                        <p style="text-align: justify; font-size: 13px;">{{substr($aArt->arts->tagline, 0, 45)}}<span class="text-theme icon-arrow-right"></span></p>
                    </a>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
@stop
<!-- JS FOR THIS PAGE -->
@section('js')
@stop
