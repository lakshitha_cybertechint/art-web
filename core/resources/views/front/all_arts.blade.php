@extends('layouts.front.master')
@section('pageTitle','Gallery')

<style type="text/css">
.util-bar.semi-compact {
    width: 120%!important;
}
.search-bar form{
   max-width: 960px!important;
}
</style>

@section('css')

@stop


<!-- BODY -->

@section('content')

      <!-- All arts data -->

      <div id="top" class="page theme-c  cbg_white" aria-hidden="false">

            <div class="page-body has-footer">

            <div class="page-header bg-theme has-image">
              <div data-role="lazy-image" class="image-lazy corner-top-left width-full height-full loaded" data-lazy-image-scale="true" data-lazy-image-animate="true" alt="" aria-hidden="true" data-lazy-image-url="//lh5.ggpht.com/5QuxTHmKR1srOJS_yQwyGqEfcc8AA30P61uYuaU39tMA2KW-eM1aXMjTLTAZp-8ztW0z-q5aIUde2vo8SSKlAdDVHE8VMZUJ-JXKtRNn0BE" data-lazy-image-width="4000" data-lazy-image-height="1269" style="background-image: url(&quot;//lh5.ggpht.com/5QuxTHmKR1srOJS_yQwyGqEfcc8AA30P61uYuaU39tMA2KW-eM1aXMjTLTAZp-8ztW0z-q5aIUde2vo8SSKlAdDVHE8VMZUJ-JXKtRNn0BE=s1600&quot;);"></div>
              <header class="page-unit">
                @if ($query == "")
                  <h1 class="hero-title">Gallery</h1>
                @else
                  <h1 class="hero-title">Gallery {{$query}}</h1>
                @endif

                <nav class="search-bar  util-bar semi-compact">
                  <form class="item" method="GET" action="{{url('arts/search')}}">
                    <input class="search-bar-query light rounded-left text-ellipsis" name="q" value="{{$query}}" placeholder="Search for an Art" required="" type="search">
                    <button class="search-bar-clear button icon-clear-input icon-only text-neutral text-dark-hover" type="button" data-role="clear-input"><span class="visually-hidden">Clear search</span></button>
                    <button class="search-bar-submit button dark-hover  icon-search rounded-right" type="submit" formnovalidate=""><span class="">Search</span></button>
                  </form>
                </nav>
              </header>
            </div>



            <!-- Cutomized Filter panel -->
        <a id="filter-toggle" class="menu-button corner-middle-left z-extra-high button huge bold square c-blue dark-hover icon-after icon-arrow-right" aria-controls="filter" aria-expanded="false" tabindex="0">
          <span class="menu-button-text ss-hidden">Filter</span>
        </a>


        <nav class="sidebar" aria-hidden="true" tabindex="-1">


        <div class="main-navigation">
        <ul class="list-links list-plain">


        </ul>


      </div>


    </nav>

    <!-- End Cutomized Filter panel -->



        <div data-role="facets-and-results">
          <div class="uhm" data-role="search-results-container">

            <div data-role="search-page " class="search-page">
              <div class="filters" data-role="filters" id="filters" aria-labelledby="filter-result-text" aria-hidden="false">
            <a href="#" class="open-filters button huge square icon-before icon-arrow-up" data-role="open-filters" aria-controls="filters" aria-expanded="false" tab-index="0" data-tab-inverted="" tabindex="-1"><span class="visually-hidden">Open </span>filters</a>
            <header class="filters-header">
              <div class="result-count text-bold" id="filter-result-text">
                  <p>3720 results found</p>

              </div>
              <button class="button square huge icon-arrow-down" data-role="close-filters" tabindex="0"><span class="visually-hidden">Close </span><span class="label-close-filters">filters</span></button>
            </header>
            <div id="filter" class="filter-container" data-role="filter-container">
              <ul class="search-filters list-plain">
                    <li class="open">
                      <header>
                        <h5>Tags</h5>
                      </header>
                      <section class="filter-body">
                          <div class="artist-suggest" data-role="artist-suggest">
                            <!-- <div class="artist-suggest-input-wrapper">
                              <input data-role="artist-suggest-input" class="facet-search ui-autocomplete-input" id="artist-suggest-input" tabindex="0" placeholder="Find Tags" autocomplete="off" type="search">
                              <button class="search-bar-clear button icon-clear-input icon-only text-neutral text-dark-hover" type="button" data-role="clear-search-filter-input" tabindex="0"><span class="visually-hidden">Clear find tags</span></button>
                            </div> -->

                            <!-- <div class="artist-suggest-list search-facets list-plain" id="artist-suggest-output"><ul class="ui-autocomplete ui-front ui-menu ui-widget ui-widget-content" id="ui-id-2" tabindex="0" style="display: none;"></ul></div> -->
                          </div>

                        <ul class="search-facets list-plain hasShowMore " aria-hidden="false" data-role="artist-filter">
                          @foreach ($artTags as $key => $artTag)

                            <li class="has-results"><a href="{{url('tag-search/'.$artTag->tag)}}"><label for="fb7f57fc-53ed-463d-8f8a-c4d459f11b9d">{{$artTag->tag}}<span>&nbsp;</span></label></a></li>
                          @endforeach

                        </ul>
                        <div class="show-more-wrapper">
                            <button class="transparent text-theme text-bold text-uppercase icon-before icon-plus" data-role="show-more" data-loading-text="Loading results..." tabindex="0">Show more</button>
                          </div>
                          <div class="show-less-wrapper">
                            <button class="transparent text-theme text-bold text-uppercase icon-before icon-minus" data-role="show-less" data-loading-text="Loading results..." tabindex="0">Show Less</button>
                          </div>
                      </section>
                    </li>

              </ul>
            </div>
          </div>

          <!-- <script>
                      window.facetJson = [{"label":"Vincent van Gogh (996)","value":"Vincent van Gogh","docCount":996,"url":"?q=&artist=Vincent van Gogh"},];

          </script> -->

              <div class="search-results-wrapper" data-role="search-results-wrapper">

                <div class="filters-bar">
                  <div class="open-filters-buttton-wrapper">
                    <a id="filter-toggle_m" href="#" class="open-filters button huge square icon-before icon-arrow-right" data-role="open-filters" aria-controls="filters" aria-expanded="false"><span class="visually-hidden">Open </span>filters</a>
                  </div>
                </div>
                <div class="page-unit">

                    <h2 class="item h-overview" role="status" aria-live="polite">
                      @if ($query == "")
                        Gallery collection
                      @else
                           {{$arts->total()}} results in the collection
                      @endif
                    </h2>

                    <div id="searchresults" class="searchresults" data-role="searchresults" data-querystring="q=" data-page-size="21">


                    <ul class="cols cols-4up list-plain">
                          @foreach ($arts as $art)
                            <li class="col lazy-content-loaded" tabindex="-1" data-no-tab="">
                              <a href="#" id="s0032V1962" class="anchor" tabindex="-1" aria-hidden="true" data-no-tab="">s0032V1962</a>
                              <a class="link-teaser triggers-slideshow-effect" href="{{url('art/'.$art->id)}}" tabindex="0">
                                <div  class="image-lazy image-large  loaded" style="background-image: url({{url($art->image)}}); background-position: center;background-size: cover;"></div>

                                <h3 style="text-align: justify; font-size: 15px;" class="text-base text-dark">{{substr($art->name, 0, 45)}}...</h3>
                                <p style="text-align: justify; font-size: 13px;">{{substr($art->tagline, 0, 45)}}...<span class="text-theme icon-arrow-right"></span></p>
                              </a>
                            </li>
                          @endforeach

                    </ul>


  <div class="text-center">
@if(isset($query) && count($arts) > 0)
{!! $arts->appends(['q' => $query])->render() !!}
@elseif (count($arts) > 0)
  {!! $arts->render() !!}
@endif

  </div>
                  </div>
                </div>
              </div>
              <div class="search-results-overlay" data-role="close-filters">

              </div>
            </div>
          </div>
          </div>

<!-- End All Arts Details -->

          </div>
    </div>

<script>
  $("#filter").hide(); //hide when loading
  $(document).ready(function(){
      $("#filter-toggle").click(function(){
          $("#filter").slideToggle();

      });
      $("#filter-toggle_m").click(function(){
          $("#filter").show();

      });
  });
</script>
@stop
<!-- JS FOR THIS PAGE -->
@section('js')

@stop
