@extends('layouts.front.master')
@section('pageTitle', 'Single Art')

<!-- CSS FOR THIS PAGE -->
@section('css')
  <script type="text/javascript">
    var imageSrc = "{{url($art->image)}}";
  </script>

<!-- <style type="text/css">
  .art-object-page.width-full.height-full.relative {overflow: hidden;}
</style> -->

<!-- FINE ZOOM -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/front/js/FineZoom/style.css')}}" />
<script type="text/javascript" src="{{asset('assets/front/js/FineZoom/jquery.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front/js/FineZoom/finezoom.js')}}"></script>

<style type="text/css">
  .fz-container{
    width: 100%!important;
    text-align: center!important;
  }
  #zoom_image{
   position: relative;
   /*max-width: 100%!important;*/
  }
  .center {
    display: block!important;
    margin-left: auto!important;
    margin-right: auto!important;
    margin-top: auto!important;
    margin-bottom: auto!important;
    /*width: 50%!important;*/
}
</style>

@stop


<!-- BODY -->

@section('content')

<div id="top" class="page theme-dark" aria-hidden="false">


    <div data-role="art-object-page" class="art-object-page width-full height-full relative">


      <div data-role="art-object-header" class="art-object-header width-full height-full relative z-low bg-theme">


        <nav data-role="object-overlay" class="back-button next-to-menu-button">
          <a id="search" data-role="menu-toggle" class="button translucent dark icon-before icon-arrow-left">Search the collection</a>
        </nav>

        <!-- Single art image data -->
          <!-- <div data-role="micrio"
               data-id="6227198725849088"
               data-base-path="https://vangoghmuseum-assetserver.appspot.com/tiles?id="
               data-init-type="cover"
               data-offset-x="50"
               data-offset-y="50"
               data-hook-events="True"
               class="width-full height-full relative z-low"></div> -->



              <div class="center" id="zoom-area">
                <!-- <img class="micrio width-full height-full relative" id="zoom_image" src="{{url($art->image)}}"/> -->

              </div>
                <img class="center" style="" id="zoom_image" src="{{url($art->image)}}" />
              <script>
                  $('#zoom_image').finezoom();
              </script>




      <div class="image-unavailable-message relative width-full height-full">
        <p class="center-all text-dark">This image is<br />unavailable</p>
      </div>

          <div class="actions">

            <div data-role="object-overlay" class="info-plaque corner-bottom-left fit z-high">
              <div class="info-button corner-bottom-left fit">
                <a href="#info" class="button light dark-hover icon-only icon-info">
                  <span class="visually-hidden">More informations</span>
                </a>
              </div>
              <p class="info-title text-light text-shadow ss-hidden">
                <strong>{{$art->name}}</strong>
                <br />
                <em>@foreach ($art->artArtists as $artArtist)
                  {{$artArtist->artist->name}},
                @endforeach</em>
              </p>
            </div>

            <nav data-role="object-overlay" class="controls corner-bottom-right fit z-extra-high">

              <div data-role="object-controls" class="object-controls hidden">
                <ul class="util-bar toolbar">
                    <li class="item"><button data-role="zoom zoom-in" class="button square light dark-hover icon-only"><i class="fas fa-search-plus"></i></button></li>
                  <li class="item"><button data-role="zoom zoom-out" class="button square light dark-hover icon-only"><i class="fas fa-search-minus"></i></button></li>
                </ul>
              </div>


            </nav>

          </div>




      </div>

      <div class="art-object-body has-footer">

        <div data-role="info">

          <div class="">

            <div class="page-unit">
              <section class="cols cols-text">
                <article class="col">

                  <h1 class="h2">
                    <a name="info" class="text-underline-none" tabindex="0">{{$art->name}}</a>
                  </h1>
                  <p class="text-italic">
                      <p class="text-bold">
                        {{$art->tagline}}
                      </p>
                      <p><?php echo nl2br($art->art_details); ?></p>
                  </p>

                    <hr class="reset-left" />

                  <p style="text-align: justify;"><?php echo $art->description; ?></p>
                </article>
                  <aside class="col">
                      <h2 class="h4">Search in the collection:</h2>
                      <ul class="list-wrapping">
                        @foreach ($art->tags as $tag)
                          <li>
                            <a href="{{url('tag-search/'.$tag->tag)}}" class="button outline neutral text-titlecase" >{{$tag->tag}}</a>
                          </li>
                        @endforeach


                      </ul>
                 </aside>
              </section>
            </div>
          </div>

            <div class="page-unit">
                <p>
                  <button data-role="object-data-toggle" class="button icon-before icon-plus">More Data</button>
                </p>
              <section data-role="object-data-content" class="cols cols-text ">
                <article class="col">
                  <dl class="list-table compact">
                    <dt class="text-titlecase">ID</dt>
                    <dd>{{$art->id}}</dd>
                    <dd></dd>
                      <dt class="text-titlecase">Added</dt>
                    <dd>{{$art->created_at->toDateTimeString()}}</dd>
                    <dd></dd>
                  </dl>
                </article>
              </section>
            </div>



       </div>
      </div>

      <div class="page-overlay"></div>

      <!-- Search Bar call on Menu -->
      <script type="text/javascript">
        $("#search").click(function() {
          $("#search-toggle").click();
        });
      </script>
    @stop

@section('js')

@stop
