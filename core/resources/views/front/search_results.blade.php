@extends('layouts.front.master')
@section('pageTitle','Search Results')
<style type="text/css">
    .util-bar.semi-compact {
    width: 120%!important;
    }
    .search-bar form{
    max-width: 960px!important;
    }
</style>
<!-- CSS FOR THIS PAGE -->
@section('css')
@stop
<!-- BODY -->
@section('content')
<!-- All arts data -->
<div id="top" class="page theme-c  cbg_white" aria-hidden="false">
    <div class="page-body has-footer">
        <div class="page-header bg-theme has-image">
            <div data-role="lazy-image" class="image-lazy corner-top-left width-full height-full loaded" data-lazy-image-scale="true" data-lazy-image-animate="true" alt="" aria-hidden="true" data-lazy-image-url="//lh4.ggpht.com/aObw6_H5guaPIDoD15rjlYB7rHzDVoiSUzBsu12t9UsiZR7C4nXz78A0xLvUOdVat9UYdY6B3hJBJynVIdAnD3KSJwhQ96DCM2LVQR0DcQ" data-lazy-image-width="4000" data-lazy-image-height="1269" style="background-image: url(&quot;//lh4.ggpht.com/aObw6_H5guaPIDoD15rjlYB7rHzDVoiSUzBsu12t9UsiZR7C4nXz78A0xLvUOdVat9UYdY6B3hJBJynVIdAnD3KSJwhQ96DCM2LVQR0DcQ=s1600&quot;);"></div>
            <header class="page-unit">
                <h1 class="hero-title">Search Results</h1>
                <nav class="search-bar util-bar semi-compact">
                    <form class="item" method="GET" action="{{url('search')}}">
                        <input class="search-bar-query light rounded-left text-ellipsis" name="q" value="" placeholder="Search.." required="" type="search">
                        <button class="search-bar-clear button icon-clear-input icon-only text-neutral text-dark-hover" type="button" data-role="clear-input"><span class="visually-hidden">Clear search</span></button>
                        <button class="search-bar-submit button dark-hover  icon-search rounded-right" type="submit" formnovalidate=""><span class="">Search</span></button>
                    </form>
                </nav>
            </header>
        </div>
        <!-- Cutomized Filter panel -->
        <a id="filter-toggle" class="menu-button corner-middle-left z-extra-high button square huge bold  c-blue dark-hover icon-after icon-arrow-right" aria-controls="filter" aria-expanded="false" tabindex="0">
        <span class="menu-button-text ss-hidden">Filter</span>
        </a>
        <nav class="sidebar" aria-hidden="true" tabindex="-1">
            <div class="main-navigation">
                <ul class="list-links list-plain">
                </ul>
            </div>
        </nav>
        <!-- End Cutomized Filter panel -->
        <div data-role="facets-and-results">
            <div class="uhm" data-role="search-results-container">
                <div data-role="search-page " class="search-page">
                    <div class="search-results-wrapper" data-role="search-results-wrapper">
                        <div class="page-unit">
                            @if (((isset($artists) ? count($artists) : 0 )+ (isset($arts) ? count($arts) : 0)) == 0)
                            <h2 class="item h-overview" role="status" aria-live="polite">
                                Sorry No Results Found For Your Query
                            </h2>
                            @endif
                            @if (isset($artists) && count($artists) > 0)
                            <h2 class="item h-overview" role="status" aria-live="polite">
                                {{$artists->count()}} artists in the results
                            </h2>
                            <div id="searchresults" class="searchresults" data-role="searchresults" data-querystring="q=" data-page-size="21">
                                <section class="cols cols-4up">
                                    @foreach ($artists->sortByDesc('created_at') as $artist)
                                    <article class="col">
                                        <a class="link-teaser" href="{{url('profile/'.$artist->id)}}">
                                            <div class="image-large has-ribbon">
                                                <img  src="{{$artist->image == "" ? url('assets/front/img/default_artist.png') : url($artist->image)}}" class="image-lazy loaded" style="">
                                                <span class="ribbon">{{$artist->name}}</span>
                                            </div>
                                            <p style="text-align: justify; font-size: 13px;">{{substr($artist->tagline, 0, 45)}}<span class="text-theme icon-arrow-right"></span></p>
                                        </a>
                                    </article>
                                    @endforeach
                                </section>
                            </div>
                            @endif
                            @if (isset($arts) && count($arts) > 0)
                            <h2 class="item h-overview" role="status" aria-live="polite">
                                {{$arts->count()}} arts in the results
                            </h2>
                            <div id="searchresults" class="searchresults" data-role="searchresults" data-querystring="q=" data-page-size="21">
                                <ul class="cols cols-4up list-plain">
                                    @foreach ($arts as $art)
                                    <li class="col lazy-content-loaded" tabindex="-1" data-no-tab="">
                                        <a href="#" id="s0032V1962" class="anchor" tabindex="-1" aria-hidden="true" data-no-tab="">s0032V1962</a>
                                        <a class="link-teaser triggers-slideshow-effect" href="{{url('art/'.$art->id)}}" tabindex="0">
                                            <div  class="image-lazy image-large  loaded" style="background-image: url({{url($art->image)}}); background-position: center;background-size: cover;"></div>
                                            <h3 style="text-align: justify; font-size: 15px;" class="text-base text-dark">{{substr($art->name, 0, 45)}}</h3>
                                            <p style="text-align: justify; font-size: 13px;">{{substr($art->tagline, 0, 45)}}<span class="text-theme icon-arrow-right"></span></p>
                                        </a>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="search-results-overlay" data-role="close-filters"></div>
                </div>
            </div>
        </div>
        <!-- End All Arts Details -->
    </div>
</div>
<script>
    $("#filter").hide(); //hide when loading
    $(document).ready(function(){
        $("#filter-toggle").click(function(){
            $("#filter").slideToggle();
    
        });
        $("#filter-toggle_m").click(function(){
            $("#filter").show();
    
        });
    });
</script>
@stop
<!-- JS FOR THIS PAGE -->
@section('js')
@stop