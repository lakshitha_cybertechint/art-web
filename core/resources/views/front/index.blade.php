@extends('layouts.front.master') @section('title','Welcome | www.cybertech.com')

<!-- CSS FOR THIS PAGE -->
@section('css')
<style type="text/css">
</style>
@stop
<!-- BODY -->

@section('content')
  <div id="top" class="page theme-dark" aria-hidden="false">


  <div data-role="art-object-page" class="art-object-page width-full height-full relative">


    <div data-role="art-object-header" class="art-object-header width-full height-full relative z-low bg-theme">
      <nav data-role="object-overlay" class="back-button next-to-menu-button">
        <a id="search" data-role="menu-toggle" class="button translucent dark icon-before icon-arrow-left">Search the collection</a>
      </nav>


    <!-- Latest arts data -->

    <div id="top" class="page theme-c  cbg_white" aria-hidden="false">

          <div class="page-body has-footer">

          <div class="page-header bg-theme has-image">
            <div data-role="lazy-image" class="image-lazy corner-top-left width-full height-full loaded" data-lazy-image-scale="true" data-lazy-image-animate="true" alt="" aria-hidden="true" data-lazy-image-url="//lh5.ggpht.com/5QuxTHmKR1srOJS_yQwyGqEfcc8AA30P61uYuaU39tMA2KW-eM1aXMjTLTAZp-8ztW0z-q5aIUde2vo8SSKlAdDVHE8VMZUJ-JXKtRNn0BE" data-lazy-image-width="4000" data-lazy-image-height="1269" style="background-image: url(&quot;//lh5.ggpht.com/5QuxTHmKR1srOJS_yQwyGqEfcc8AA30P61uYuaU39tMA2KW-eM1aXMjTLTAZp-8ztW0z-q5aIUde2vo8SSKlAdDVHE8VMZUJ-JXKtRNn0BE=s1600&quot;);"></div>
            <header class="page-unit">
              <h1 class="hero-title">Latest Arts</h1>
            </header>

          </div>


          <div class="page-unit width-full width-none">

              <section class="cols cols-text text-center">


                    <p class="text-intro text-theme">Find out what are the latest arts at the Art Web.</p>


              </section>


              <section class="cols cols-5up">
                @foreach ($latestArts as $lArt)
                  <article class="col">
                      <a class="link-teaser" href="{{url('art/'.$lArt->id)}}">
                        <div class="image-large has-ribbon" style="background-image: url({{url($lArt->image)}}); background-position: center;background-size: cover;">

                          {{-- <img data-role="lazy-image" class="image-lazy loaded" data-lazy-image-scale="true" data-lazy-image-animate="true" src="{{url($lArt->image)}}" alt="" aria-hidden="true"  width="3508" height="200" > --}}


                            {{-- <span class="ribbon">Now on view</span> --}}
                        </div>
                        <h3 style="text-align: justify; font-size: 15px;" class="text-base text-dark">{{substr($lArt->name, 0, 45)}}</h3>
                        <p style="text-align: justify; font-size: 13px;">{{substr($lArt->tagline, 0, 45)}}<span class="text-theme icon-arrow-right"></span></p>
                      </a>
                  </article>
                @endforeach


              </section>



          </div>

          <!-- Latest Artists -->
          <div class="page-header bg-theme has-image">
              <div data-role="lazy-image" class="image-lazy corner-top-left width-full height-full loaded" data-lazy-image-scale="true" data-lazy-image-animate="true" alt="" aria-hidden="true" data-lazy-image-url="//lh3.googleusercontent.com/pmIC6ZwXQrxyXcgrJ6go0i0XOvl_84teCf-P2o9mC46OXaZx32S_dz21gfCSc6h4vHLG6JVwUKdyU-yq7AoK9c30VORVE2LHC4i7igN1kw4" data-lazy-image-width="4000" data-lazy-image-height="1269" style="background-image: url(&quot;//lh3.googleusercontent.com/pmIC6ZwXQrxyXcgrJ6go0i0XOvl_84teCf-P2o9mC46OXaZx32S_dz21gfCSc6h4vHLG6JVwUKdyU-yq7AoK9c30VORVE2LHC4i7igN1kw4=s1600&quot;);"></div>

            <header class="page-unit">
              <h1 class="hero-title">Latest Artists</h1>
            </header>

          </div>

          <div class="page-unit width-full width-none">

              <section class="cols cols-text text-center">


                    <p class="text-intro text-theme">Find out what are the latest artists at the Art Web.</p>


              </section>

              <section class="cols cols-5up">
                @foreach ($latestArtists as $artist)
                  <article class="col">
                      <a class="link-teaser" href="{{url('profile/'.$artist->id)}}">
                        <div class="image-large has-ribbon">

                          <img  src="{{$artist->image == "" ? url('assets/front/img/default_artist.png') : url($artist->image)}}" class="image-lazy loaded" style="">


                            <span class="ribbon">{{$artist->name}}</span>
                        </div>
                        <p style="text-align: justify; font-size: 13px;">{{substr($artist->tagline, 0, 45)}}<span class="text-theme icon-arrow-right"></span></p>
                      </a>
                  </article>
                @endforeach


              </section>


          </div>

        </div>
  </div>





     <!-- End Latest arts -->

   </div> <!-- end art-object-header -->

  </div><!-- end art-object-page  -->


  </div> <!-- end page -->

  <div class="page-overlay"></div>

  <!-- Search Bar call on Menu -->
  <script type="text/javascript">
    $("#search").click(function() {
      $("#search-toggle").click();
    });
  </script>

@stop

@section('js')

@stop
