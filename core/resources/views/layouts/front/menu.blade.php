<a data-role="menu-toggle" class="menu-button corner-top-left z-extra-high button huge bold square light dark-hover" href="#menu" aria-controls="menu" aria-expanded="false" tabindex="0">
  <div class="hamburger">
    <div class="toppings"></div>
    <div class="burger"></div>
    <div class="bun"></div>
  </div>
  <span class="menu-button-text ss-hidden">Menu</span>
</a>

<nav id="menu" class="sidebar" aria-hidden="true" tabindex="-1">
<div data-role="top-navigation" class="top-navigation">
<!--  <a href="https://vangoghmuseum.nl/nl/collectie/index" class="menu-button button square huge light-hover ss-hidden "><span aria-hidden="true">NL</span><span class="visually-hidden">Nederlands</span></a>
<a href="index.html" class="menu-button button square huge light-hover ss-hidden active"><span aria-hidden="true">EN</span><span class="visually-hidden">English</span></a>

<button data-role="language-toggle" class="language-toggle button square huge light-hover icon-after icon-arrow-down" aria-controls="language-list" aria-expanded="false">
  <span class="ss-hidden">Other </span>languages
</button> -->

<div id="menu-search-form" class="search-form" aria-hidden="true">
  <form data-role="search-form" action="{{url('search')}}" method="GET">
    <input data-role="search-input" type="search" name="q" class="huge width-full square"
           placeholder="Search the website " tabindex="-1" />
    <button class="search-bar-clear button icon-clear-input icon-only text-neutral text-light-hover" type="button" data-role="clear-input" tabindex="-1"><span class="visually-hidden">Zoek leegmaken</span></button>
  </form>
</div>

<button id="search-toggle" data-role="search-toggle" class="search-glass menu-button button square huge light-hover icon-only icon-search" aria-controls="menu-search-form" aria-expanded="false"><span class="visually-hidden">Zoeken</span></button>

</div>
<div class="main-navigation">
<ul class="list-links list-plain">

<li>
  <a class="text-bold text-light">Welcome to Art Gallery!</a>
</li>

<li>
  <a class="text-bold text-light" href="{{url("/")}}">Home</a>
</li>

<li>
  <a class="text-bold text-light" href="{{url("arts")}}">Gallery</a>
</li>

<!-- <li>
  <a class="text-bold text-light" href="{{url("artists")}}">Arists</a>
</li> -->
<li>
          <a class="text-bold text-light icon-after icon-arrow-right" data-role="sub-menu-toggle" href="{{url("artists")}}" aria-controls="sub-nav-2" aria-expanded="true" tabindex="0">Arists</a>

          <ul class="list-links list-plain" id="sub-nav-2" aria-hidden="false">
            <li>
	            <a class="text-light-hover" href="{{url("artists")}}" tabindex="0">
	                All Artists
	            </a>
	        </li>
            @foreach (ArtistManage\Models\Artist::whereStatus(1)->orderby("name")->get() as $artist)
              <li>
                <a class="text-light-hover" href="{{url('profile/'.$artist->id)}}" tabindex="0">
                    {{$artist->name}}
                </a>
              </li>
              @endforeach

          </ul>
        </li>

{{-- <li>
  <a class="text-bold text-light">&nbsp;</a>
</li> --}}

{{-- <li>
  <a class="text-bold text-light" href="{{url("artists")}}">Listed artists</a>
</li> --}}
<!-- List of Artists -->
{{-- @foreach (ArtistManage\Models\Artist::whereStatus(1)->get() as $artist) --}}
  {{-- <li>
    <a class="text-light-hover" id="artist_{{$artist->id}}" href="{{url('profile/'.$artist->id)}}">
      {{$artist->name}}
    </a>
  </li> --}}
{{-- @endforeach --}}

</ul>

<!-- <div class="bottom-navigation">
  <div class="social-links">
      <a href="https://twitter.com/">
        <i class="fab fa-twitter-square"></i>
      </a>
      <a href="https://www.facebook.com/">
        <i class="fab fa-facebook-square"></i>
      </a>
      <a href="https://www.google.com/">
        <i class="fab fa-google-plus-square"></i>
      </a>
      <a href="https://www.instagram.com/">
        <i class="fab fa-instagram"></i>
      </a>
  </div>

  <div class="footer-links">
      <a href="/contact">Contact</a>
      <a href="/newsletter">Newsletter</a>
      <a href="/privacy">Privacy &amp; Cookies</a>
  </div>
</div> -->

</div>


</nav>
