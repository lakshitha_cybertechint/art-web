<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
  <meta charset="utf-8">
  <meta name="google" value="notranslate">
  <meta http-equiv="Content-Language" content="en_US" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />

  <link href="assets/img/touch-icons/apple-touch-icon.png" rel="apple-touch-icon" />
  <link href="assets/img/touch-icons/apple-touch-icon-152x152.png" rel="apple-touch-icon" sizes="152x152" />
  <link href="assets/img/touch-icons/apple-touch-icon-167x167.png" rel="apple-touch-icon" sizes="167x167" />
  <link href="assets/img/touch-icons/apple-touch-icon-180x180.png" rel="apple-touch-icon" sizes="180x180" />
  <link href="assets/img/touch-icons/icon-hires.png" rel="icon" sizes="192x192" />
  <link href="assets/img/touch-icons/icon-normal.png" rel="icon" sizes="128x128" />

  <title>The Art Web</title>

  <meta name="description" content="The Art Web" />

  <meta property="og:image" content="" />
  <meta property="og:title" content="The Art Web" />
  <meta property="og:description" content="The Art Web" />
  <meta property="og:url" content="" />

  <link rel="shortcut icon" href="https://vangoghmuseum.nl/favicon.ico" />

  <link rel="stylesheet" type="text/css" href="{{url('assets//front/js/slick1.5.7/slick.min.js')}}" />

  <!-- Font Awesome CSS -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

  <!-- Main CSS -->
  <link rel="stylesheet" href="{{url('assets/front/css/fonts.css')}}" />
  <link href="{{url('assets/front/css/van-gogh.styles.css')}}" rel="stylesheet"/>


  <script src="{{asset('assets/front/js/generated/modernizr.js')}}"></script>

  <!-- JQuery 3.3.1-->
  <script src="{{asset('assets/front/js/JQuery3.3.1/jquery.min.js')}}"></script>

  <!--[if lte IE 9]> <link href="/assets/outdatedbrowser/outdatedbrowser.css" rel="stylesheet"/>
 <![endif]-->



    @yield('css')

    </head>
    <body class="theme-override loading">

    <div class="viewport">
      @include('layouts.front.menu')

    @yield('content')


    </div> <!-- End viewport -->


    <!-- Application scripts -->
    <script src="{{url('assets/front/js/generated/van-gogh.js')}}"></script>


    <!-- Optional scripts -->

    <script type="text/javascript" src="{{url('assets/front/js/slick1.5.7/slick.min.js')}}"></script>


  <!--[if gte IE 9]>
  <script src="{{asset('assets/front/js/generated/van-gogh-vue.js')}}"></script>
  <!-->


  </body>

  </html>

    @yield('js')
