<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/**
 * USER AUTHENTICATION MIDDLEWARE
 */

/*DONT USE THIS ROUTE FOR OTHER USAGE ----ONLY FOR THIS */
Route::group(['middleware' => ['auth']], function()
{
    Route::get('admin', [
      'as' => 'dashboard', 'uses' => 'WelcomeController@index'
    ]);

});


Route::group([], function()
{
    Route::get('/', [
      'as' => 'index', 'uses' => 'WebController@index'
    ]);

});

/* ROUTES FOR PAGES */
Route::get('/arts', [
  'as' => 'all_arts', 'uses' => 'ArtController@index'
]);

Route::get('/artists', [
  'as' => 'all_artists', 'uses' => 'ArtistController@index'
]);
Route::get('/artists/search', 'ArtistController@searchResults');
Route::get('/arts/search', 'ArtController@searchResults');

Route::get('/art/{id}', [
  'as' => 'single_art', 'uses' => 'ArtController@show'
]);

Route::get('/profile/{id}', [
  'as' => 'atrtist_profile', 'uses' => 'ArtistController@show'
]);

Route::get('/search', 'WebController@searchResults');
Route::get('/tag-search/{q}', 'WebController@searchByTagResults');

Route::get('get-image/{id}', function(){
  return '{
  "levels": [
    {
      "name": "z0",
      "width": 6917,
      "height": 5366,
      "tiles": [
        {
          "x": 0,
          "y": 0,
          "url": "http://lh6.ggpht.com/N-Q2hQ6k_hWlZ0P1G29kxzKyencRdk78xNB1ngDuLFdCgXrbi-kKVWyHdnbpgFBQL0Y4ZkUNaOmRolCJwCyu9X7icJiwXG7XvsB8RsEQ1w"
        }
      ]
    }
  ]
}';
});


/**
 * USER REGISTRATION & LOGIN
 */

Route::get('user/login', [
  'as' => 'user.login', 'uses' => 'AuthController@loginView'
]);
Route::post('user/login', [
  'as' => 'user.login', 'uses' => 'AuthController@login'
]);

Route::get('user/logout', [
  'as' => 'user.logout', 'uses' => 'AuthController@logout'
]);



/**
 * USER LOGIN VIA FACEBOOK/GOOGLE/TWITTER/LINKDIN ETC
 */

Route::get('auth/facebook', 'AuthController@redirectToFacebook');
Route::get('auth/facebook/callback', 'AuthController@handleFacebookCallback');

Route::get('auth/google', 'AuthController@redirectToGoogle');
Route::get('auth/google/callback', 'AuthController@handleGoogleCallback');
