<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use ArtManage\Models\Art;
use ArtManage\Models\ArtTag;

class ArtController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $arts = Art::whereStatus(1)
      ->paginate(20);

      $artTags = ArtTag::select('tag')
      ->groupBy('tag')
      ->orderBy('tag')
      ->get();
        return view('front.all_arts')
        ->with('artTags', $artTags)
        ->with('query', request()->q)
        ->with('arts', $arts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $art = Art::where('status', 1)->whereId($id)->first();
      if ($art) {
        return view('front.single_art')
        ->with('art', $art);
      }

      return redirect('arts');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function searchResults()
    {
        $queries = explode(' ', request()->q);
        $queries = array_filter(array_map('trim', $queries)); // removing spaces and space array values

        $arts = [];
        if (count($queries) > 0) {

            // start artists
            $arts = \ArtManage\Models\Art::whereStatus(1)->has('artArtists');

            foreach ($queries as $key =>$query) {
                $arts = $arts->where('name', 'LIKE', '%' . $query . '%')
                ->orWhereHas('tags', function($q1) use ($query){
                  $q1->where('tag', 'LIKE', '%' . $query . '%');
                });
            }
            $arts = $arts->paginate(20);
            // end artists
        }

        $artTags = ArtTag::select('tag')
        ->groupBy('tag')
        ->orderBy('tag')
        ->get();

        return view('front.all_arts')
        ->with('artTags', $artTags)
        ->with('arts', $arts)
        ->with('query', request()->q);
    }
}
