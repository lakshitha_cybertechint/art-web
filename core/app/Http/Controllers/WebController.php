<?php

namespace App\Http\Controllers;

use Sentinel;
use Permissions\Models\Permission;
use ArtManage\Models\Art;
use ArtistManage\Models\Artist;

class WebController extends Controller
{

    /*
        |--------------------------------------------------------------------------
        | Web Controller
        |--------------------------------------------------------------------------
        |
        | This controller renders the "marketing page" for the application and
        | is configured to only allow guests. Like most of the other sample
        | controllers, you are free to modify or remove it as you desire.
        |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
    }



    public function index()
    {
        $latestArts =  Art::orderBy('created_at', 'DESC')
            ->whereStatus(1)
            ->limit(5)
            ->get();
        $latestArtists =  Artist::orderBy('created_at', 'DESC')
            ->whereStatus(1)
            ->limit(5)
            ->get();
        return view('front.index')
                ->with("latestArtists", $latestArtists)
                ->with("latestArts", $latestArts);
    }

    public function searchResults()
    {
        $queries = explode(' ', request()->q);
        $queries = array_filter(array_map('trim', $queries)); // removing spaces and space array values

        $arts = [];
        $artists = [];
        if (count($queries) > 0) {
            // start arts
            $arts = \ArtManage\Models\Art::whereStatus(1)->has('artArtists');

            foreach ($queries as $key =>$query) {
                $arts = $arts->where('name', 'LIKE', '%' . $query . '%')
                ->orWhereHas('tags', function($q1) use ($query){
                  $q1->where('tag', 'LIKE', '%' . $query . '%');
                });
            }
            $arts = $arts->get();
            // end arts

            // start artists
            $artists = \ArtistManage\Models\Artist::whereStatus(1)->has('artistArts');

            foreach ($queries as $key =>$query) {
                $artists = $artists->where('name', 'LIKE', '%' . $query . '%');
            }
            $artists = $artists->get();
            // end artists
        }

        return view('front.search_results')
                ->with('artists', $artists)
                ->with('query', request()->q)
                ->with('arts', $arts);
    }
    public function searchByTagResults($q)
    {
        $queries = explode(' ', $q);
        $queries = array_filter(array_map('trim', $queries)); // removing spaces and space array values

        $arts = [];
        $artists = [];
        if (count($queries) > 0) {
            // start arts
            $arts = \ArtManage\Models\Art::whereStatus(1)->has('artArtists');

            foreach ($queries as $key =>$query) {
                $arts = $arts
                ->whereHas('tags', function($q1) use ($query){
                  $q1->where('tag', 'LIKE', $query);
                });
            }
            $arts = $arts->get();
            // end arts


        }

        return view('front.search_results')
                ->with('query', $q)
                ->with('arts', $arts);
    }
}
