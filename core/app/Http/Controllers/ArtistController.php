<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use ArtistManage\Models\Artist;

class ArtistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $artists = Artist::whereStatus(1)
      ->with("artistArts")
      ->orderBy("name", "DESC")
      ->paginate(20);

      $filter = [];

      // $artists->each(function($artist){
      //   $filter[] = [
      //     "label" => $artist->name." (".$artist->arts->count().")",
      //     "value" => $artist->name,
      //     "docCount" => $artist->arts->count(),
      //     "url" => url("/profile/".$artist->id),
      //     "id" => $artist->id
      //   ];
      // });

      return view('front.all_artists')
      ->with('artists', $artists)
      ->with('query', request()->q);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $artist = Artist::with('artistArts.arts')
      ->whereStatus(1)
      ->whereId($id)
      ->first();
        return view('front.atrtist_profile')
        ->with('artist', $artist);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


      public function searchResults()
      {
          $queries = explode(' ', request()->q);
          $queries = array_filter(array_map('trim', $queries)); // removing spaces and space array values

          $artists = [];
          if (count($queries) > 0) {

              // start artists
              $artists = \ArtistManage\Models\Artist::whereStatus(1)->has('artistArts');

              foreach ($queries as $key =>$query) {
                  $artists = $artists->where('name', 'LIKE', '%' . $query . '%');
              }
              $artists = $artists->paginate(20);
              // end artists
          }

          return view('front.all_artists')
          ->with('artists', $artists)
  				->with('query', request()->q);
      }


}
