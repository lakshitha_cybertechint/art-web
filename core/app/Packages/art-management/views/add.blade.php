@extends('layouts.back.master') @section('current_title','New Art')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
{{-- <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" /> --}}
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all" />
<style media="screen">
  .select2-input{
    width: auto !important;
  }
</style>
@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
    <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('admin/arts/list')}}">Art Management</a></li>

        <li class="active">
            <span>New Art</span>
        </li>
    </ol>
</div>
@stop
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">
                <form method="POST" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
                	{!!Form::token()!!}

                	<div class="form-group">
                    <label class="col-sm-2 control-label">NAME*</label>
                    	<div class="col-sm-10"><input type="text" class="form-control" name="name"></div>
                	</div>
                	<div class="form-group">
                    <label class="col-sm-2 control-label">Tagline</label>
                    <div class="col-sm-10"><textarea name="tagline" class="form-control" maxlength="150" placeholder="Maximum 150 characters are allowed"></textarea></div>
                    </div>
                	<div class="form-group">
                    <label class="col-sm-2 control-label">Art Details</label>
                         <div class="col-sm-10"><textarea id="art_details" name="art_details" class="form-control"></textarea></div>

                    </div>
                	<div class="form-group">
                    <label class="col-sm-2 control-label">Description</label>
                         <div class="col-sm-10"><textarea id="description" name="description" class="form-control"></textarea></div>

                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">Artists *</label>
                         <div class="col-sm-10">
                         <select class="js-source-states" multiple="multiple" name="artists[]" style="width: 100%">
                         <?php foreach (ArtistManage\Models\Artist::whereStatus(1)->get() as $artist): ?>
                             <option value="{{$artist->id}}">{{$artist->name}}</option>
                         <?php endforeach ?>


                        </select>
                    </div>

                	</div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label required">IMAGE * </label>
                        <div class="col-sm-10  text-right">
                            <input id="image"  name="image"  type="file" class="file-loading" onchange="dimentionsCheck()">
                            <p class="text-danger pull-left" style="font-weight: 600; font-size: 13px;">Image dimentions should be at least 1200px X 800px</p>
                        </div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">Tags</label>
                         <div class="col-sm-10">
                           <input type="text" style="width : auto !importent;"   id="id" multiple="true" name="tags"  multiple="true"  class="selec2-tags" >

                    </div>

                	</div>



                  <div class="hr-line-dashed"></div>
                  <div class="form-group">
                      <div class="col-sm-6 col-sm-offset-2 ">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                      </div>
                      <div class="col-sm-4 text-right">
                          <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
                          <button class="btn btn-primary" type="submit">Done</button>
                      </div>
                  </div>

                </form>
        </div>
    </div>
</div>
@stop
@section('js')
<script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
<script src="{{asset('assets/back/file/bootstrap-fileinput-master/js/fileinput.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(".js-source-states").select2();

		$(".selec2-tags").select2({
      tags : []
    });

        $("#image").fileinput({
            uploadUrl: "", // server upload action
            uploadAsync: true,
            maxFileCount: 1,
            showUpload:false,
            allowedFileExtensions: ["jpg", "gif", "png"]
        });

	});


</script>

<script src="//cdn.ckeditor.com/4.10.0/full/ckeditor.js"></script>
<script type="text/javascript">
CKEDITOR.replace( 'description', {} );
</script>

<script type="text/javascript">
  function dimentionsCheck() {
    var fileInput = $.find("input[name='image']")[0],
       file = fileInput.files && fileInput.files[0];

   if( file ) {
       var img = new Image();

       img.src = window.URL.createObjectURL( file );

       img.onload = function() {
           var width = img.naturalWidth,
               height = img.naturalHeight;

           window.URL.revokeObjectURL( img.src );

           if( width > 1195 && height > 795 ) {
               console.log("ok");
           }
           else {
              // $('#image').fileinput('clear');
              $('#image').fileinput('reset');
              $('#image').fileinput('refresh');
              alert("Minimum image dimension is 1200px X 800px");
           }
       };
   }
   else { //No file was input or browser doesn't support client side reading
       console.log("No support");
   }
  }

  $('#art_details').keypress(function(event) {
     if (event.which == 13) {
        event.stopPropagation();
     }
  });​
</script>

@stop
