<?php
namespace ArtManage\Models;

use Illuminate\Database\Eloquent\Model;


class Art extends Model{



	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name','image','description','status', 'tagline', 'art_details'];

public function artArtists()
{
	return $this->hasMany(ArtArtist::class);
}

public function tags()
{
	return $this->hasMany(ArtTag::class);
}

}
