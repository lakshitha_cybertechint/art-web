<?php
namespace ArtManage\Models;

use Illuminate\Database\Eloquent\Model;

class ArtArtist extends Model{


	protected $fillable = ['art_id','artist_id'];

public function artist()
{
	return $this->belongsTo(\ArtistManage\Models\Artist::class);
}

public function arts()
{
	return $this->belongsTo(\ArtManage\Models\Art::class, 'art_id');
}

public function activeArts()
{
	return $this->belongsTo(\ArtManage\Models\Art::class, 'art_id')->whereStatus(1);
}

}
