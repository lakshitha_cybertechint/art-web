<?php
Route::group(['middleware' => ['auth']], function () {
    Route::group(['prefix' => 'admin/arts/', 'namespace' => 'ArtManage\Http\Controllers'], function () {
        Route::get('list', [
        'as' => "gallery.list", 'uses' => 'ArtController@viewList'
      ]);
        Route::get('json/list', [
        'as' => "gallery.list", 'uses' => 'ArtController@getList'
      ]);

      Route::get('add', [
      'as' => "gallery.list", 'uses' => 'ArtController@addView'
    ]);
      Route::post('add', [
      'as' => "gallery.list", 'uses' => 'ArtController@store'
    ]);

      Route::get('edit/{id}', [
      'as' => "gallery.list", 'uses' => 'ArtController@show'
    ]);

      Route::post('edit/{id}', [
      'as' => "gallery.list", 'uses' => 'ArtController@update'
    ]);

    Route::post('delete', [
      'as' => "gallery.list", 'uses' => 'ArtController@delete'
    ]);
    });
});
