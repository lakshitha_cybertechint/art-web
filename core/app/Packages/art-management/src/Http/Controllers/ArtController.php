<?php
namespace ArtManage\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use ArtManage\Models\Art;
use ArtManage\Models\ArtTag;
use ArtManage\Models\ArtArtist;
use Sentinel;
use Response;
use File;


class ArtController extends Controller {
	public function viewList()
	{
		return view("ArtManage::list");
	}

	public function getList()
	{

			if(request()->ajax()){
				$data= Art::with("artArtists.artist")->where('status', '!=', -1)->get();
				$jsonList = array();
				$i=1;
				foreach ($data as $key => $arts) {

					$dd = array();
					array_push($dd, $i);

					if($arts->name != ""){
						array_push($dd, $arts->name);
					}else{
						array_push($dd, "-");
					}
					if($arts->image != ""){
						array_push($dd, '<img src="'.url($arts->image).'" style="max-height : 100px; max-width : 200px;">');
					}else{
						array_push($dd, "-");
					}
					if(count($arts->artArtists) > 0){
						$artists = '';
						foreach ($arts->artArtists as $artAritst) {
							$artists .= $artAritst->artist->name.", ";
						}
						array_push($dd, $artists);
					}else{
						array_push($dd, "-");
					}
					if($arts->tagline != ""){
						array_push($dd, $arts->tagline);
					}else{
						array_push($dd, "-");
					}
					if($arts->created_at != ""){
						array_push($dd, $arts->created_at->toDateTimeString());
					}else{
						array_push($dd, "-");
					}

					$permissions = Permission::whereIn('name',['branch.edit','admin'])->where('status','=',1)->lists('name');
					if(Sentinel::hasAnyAccess($permissions)){
						array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\''.url('admin/arts/edit/'.$arts->id).'\'" data-toggle="tooltip" data-placement="top" title="Edit Art"><i class="fa fa-pencil"></i></a></center>');
					}else{
						array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
					}

					$permissions = Permission::whereIn('name',['branch.delete','admin'])->where('status','=',1)->lists('name');
					if(Sentinel::hasAnyAccess($permissions)){
						array_push($dd, '<center><a class="arts-delete" data-id="'.$arts->id.'" data-toggle="tooltip" data-placement="top" title="Delete Art"><i class="fa fa-trash-o"></i></a></center>');
					}else{
						array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
					}

					array_push($jsonList, $dd);
					$i++;
				}
				return Response::json(array('data'=>$jsonList));
			}else{
				return Response::json(array('data'=>[]));
			}
	}

	public function addView()
	{
		return view("ArtManage::add");
	}

	public function store()
	{
		$validator =  $this->validate(request(), $this->getRules());
		// if (correct_size(request()->image)) {
		// 	$validator->getMessageBag()->add("image", "Minimum image dimension is 1200px X 800px");
		// }
			if ($validator && $validator->fails()) {
				return redirect('admin/art/add')->withErrors($validator->errors())->with([ 'error' => true,
				'error.message'=> 'Please check the errors',
				'error.title' => 'Oops!, Something went wrong']);;
			}else{
				$tags = explode(",", request()->tags);

				$imageName =  $this->uploadImage(request()->file("image"));


				$art = Art::create(request()->all());
				$art->image = $imageName;
				$art->save();

				ArtTag::where('art_id', $art->id)->delete();
				foreach ($tags as $key => $tag) {
					ArtTag::create([
						"art_id" => $art->id,
						"tag" => $tag
					]);
				}
				ArtArtist::where('art_id', $art->id)->delete();
				foreach (request()->artists as $key => $artist) {
					ArtArtist::create([
						"art_id" => $art->id,
						"artist_id" => $artist
					]);
				}
				if ($art) {
					return redirect('admin/arts/list')->with([ 'success' => true,
						'success.message'=> 'Art Created successfully!',
						'success.title' => 'Well Done!']);
				}
			}

	}

	public function update($id)
	{
		$validator =  $this->validate(request(), $this->getRules($id));
			if ($validator && $validator->fails()) {
				return redirect('admin/art/add')->withErrors($validator->errors())->with([ 'error' => true,
				'error.message'=> 'Please check the errors',
				'error.title' => 'Oops!, Something went wrong']);;
			}else{
				$tags = explode(",", request()->tags);



				 Art::where("id", $id)->update([
					'name' => request()->name,
					'tagline' => request()->tagline,
					'art_details' => request()->art_details,
					'description' => request()->description
				]);
				$art = Art::with("artArtists.artist")->find($id);

				if (request()->hasFile("image")) {
					$imageName =  $this->uploadImage(request()->file("image"));
					$art->image = $imageName;
					$art->save();
				}

				ArtTag::whereArtId($id)->delete();

				foreach ($tags as $key => $tag) {
					ArtTag::create([
						"art_id" => $art->id,
						"tag" => $tag
					]);
				}

				ArtArtist::whereArtId($id)->delete();


				foreach (request()->artists as $key => $artist) {
					ArtArtist::create([
						"art_id" => $art->id,
						"artist_id" => $artist
					]);
				}
				if ($art) {
					return redirect('admin/arts/list')->with([ 'success' => true,
						'success.message'=> 'Art updated successfully!',
						'success.title' => 'Well Done!']);
				}
			}

	}

	public function show($id)
	{
		$art = Art::with("artArtists", "tags")->find($id);

		if ($art) {
			return view("ArtManage::edit")->with([
				"art" => $art
			]);
		}

		redirect('admin/arts/list');
	}
public function uploadImage($file)
{
	$filePath = "core/storage/uploads/arts/";
	$fileName = "arts_".rand().".".$file->getClientOriginalExtension();
	$file->move($filePath, $fileName);

	return $filePath.$fileName;
}

public function delete()
{
	$art = Art::find(request()->id);
	if ($art) {
		if ($art->update([
				'status' => -1
			])) {
			return response()->json([
				"status" => "success"
			], 200);
		}

	}
	return response()->json(["unsuccess"], 500);
}
	public function getRules($id = -1)
	{
		$rules = [
			"name" => "required|unique:arts,name,".$id,
			"artists" => "required",
			"tagline" => 'required|max:150'
		];

		if ($id == -1) {
			$rules["image"] = "required|mimes:jpeg,jpg,gif,png";
		}else {
			$rules["image"] = "mimes:jpeg,jpg,gif,png";
		}

		return $rules;
	}

	public function correct_size($photo) {
    $maxHeight = 5000;
    $maxWidth = 5000;
    list($width, $height) = getimagesize($photo);
    return ( ($width <= $maxWidth) && ($height <= $maxHeight) );
}
}
