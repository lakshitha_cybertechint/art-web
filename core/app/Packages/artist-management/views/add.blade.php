@extends('layouts.back.master')
@section('current_title','New Artist')
@section('css')
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}" />
<link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}" />
{{-- <link rel="stylesheet" type="text/css" href="{{asset('assets/back/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all" /> --}}
<style media="screen">
.imageBox
{
  position: relative;
  height: 300px;
  width: 300px;
  border:1px solid #aaa;
  background: #fff;
  overflow: hidden;
  background-repeat: no-repeat;
  cursor:move;
}

.imageBox .thumbBox
{
  position: absolute;
  top: 50%;
  left: 50%;
  width: 250px;
  height: 250px;
  margin-top: -124px;
  margin-left: -124px;
  box-sizing: border-box;
  border: 1px solid rgb(102, 102, 102);
  box-shadow: 0 0 0 1000px rgba(0, 0, 0, 0.5);
  background: none repeat scroll 0% 0% transparent;
}

.imageBox .spinner
{
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  text-align: center;
  line-height: 400px;
  background: rgba(0,0,0,0.7);
}
</style>
@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
  <ol class="hbreadcrumb breadcrumb">
    <li><a href="{{url('admin/artists/list')}}">Artist Management</a></li>

    <li class="active">
      <span>New Artist</span>
    </li>
  </ol>
</div>
@stop
@section('content')

<div class="row">
  <div class="col-lg-12">
    <div class="hpanel">
      <div class="panel-body">
        <form method="POST" class="form-horizontal" id="form" action="" enctype="multipart/form-data">
          {!!Form::token()!!}
          <input type="hidden" name="image" id="artistImage" value="">
          <div class="form-group"><label class="col-sm-2 control-label">NAME*</label>
            <div class="col-sm-10"><input type="text" class="form-control" name="name"></div>
          </div>
          <div class="form-group"><label class="col-sm-2 control-label">Date of Birth</label>
            <div class="col-sm-10"><input type="date" class="form-control" name="birth_date"></div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Tagline</label>
            <div class="col-sm-10"><textarea name="tagline" class="form-control" maxlength="150" placeholder="Maximum 150 characters are allowed"></textarea></div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label required">IMAGE</label>
            <div class="col-md-6">
            <div class="imageBox">
              <div class="thumbBox"></div>
              {{-- <div class="spinner" style="display: none">Loading...</div> --}}
            </div>
            <div class="action">
              <input type="file" id="file" style="float:left; ">
              <input type="button" id="btnCrop" value="Crop" style="float: right">
              <input type="button" id="btnZoomIn" value="+" style="float: right">
              <input type="button" id="btnZoomOut" value="-" style="float: right">
            </div>

            </div>
            <div class="col-md-4" style="margin-top : 30px;">
              <div class="cropped">

              </div>
            </div>
          </div>

          <div class="form-group"><label class="col-sm-2 control-label">Description</label>
            <div class="col-sm-10"><textarea class="form-control" id="description" name="description" rows="8" style="min-width : 100%; max-width : 100%;"></textarea> </div>
          </div>



          <div class="hr-line-dashed"></div>
          <div class="form-group">
            <div class="col-sm-6 col-sm-offset-2 ">
              @if (count($errors) > 0)
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
              @endif
            </div>
            <div class="col-sm-4 text-right">
              <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
              <button class="btn btn-primary" type="submit">Done</button>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
  @stop
  @section('js')
  <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
  {{-- <script src="{{asset('assets/back/file/bootstrap-fileinput-master/js/fileinput.min.js')}}" type="text/javascript"></script> --}}
  <script src="{{asset('assets/back/cropbox/cropbox-min.js')}}" type="text/javascript"></script>

  {{-- <script type="text/javascript">
    $("#image").fileinput({
      uploadUrl: "", // server upload action
      uploadAsync: true,
      maxFileCount: 1,
      showUpload: false,
      allowedFileExtensions: ["jpg", "gif", "png"]
    });
  </script> --}}

  <script type="text/javascript">
  $(window).load(function() {
        var options =
        {
            thumbBox: '.thumbBox',
            spinner: '.spinner',
            imgSrc: 'avatar.png'
        }
        var cropper = $('.imageBox').cropbox(options);
        $('#file').on('change', function(){
            var reader = new FileReader();
            reader.onload = function(e) {
                options.imgSrc = e.target.result;
                cropper = $('.imageBox').cropbox(options);
            }
            reader.readAsDataURL(this.files[0]);
            this.files = [];
        })
        $('#btnCrop').on('click', function(){
            var img = cropper.getDataURL();
            $("#artistImage").val(img)
            $('.cropped').html('<img style="max-width : 100%;" src="'+img+'">');
        })
        $('#btnZoomIn').on('click', function(){
            cropper.zoomIn();
        })
        $('#btnZoomOut').on('click', function(){
            cropper.zoomOut();
        })
    });
  </script>
  <script src="//cdn.ckeditor.com/4.10.0/full/ckeditor.js"></script>
  <script type="text/javascript">
  CKEDITOR.replace( 'description', {} );
  </script>
  @stop
