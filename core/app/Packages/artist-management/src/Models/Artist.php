<?php
namespace ArtistManage\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Branch Model Class
 *
 *
 * @category   Models
 * @package    Model
 * @author     Insaf Zakariya <insaf.zak@gmail.com>
 * @copyright  Copyright (c) 2015, Yasith Samarawickrama
 * @version    v1.0.0
 */
class Artist extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */


	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'birth_date', 'description', 'status', 'image', 'tagline'];

public function artistArts()
{
	return $this->hasMany(\ArtManage\Models\ArtArtist::class, 'artist_id', 'id');
}

}
