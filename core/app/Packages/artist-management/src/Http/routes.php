<?php

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'admin/artists', 'namespace' => 'ArtistManage\Http\Controllers'], function(){

      Route::get('list', [
        'as' => "gallery.list", 'uses' => 'ArtistsController@lists'
      ]);
      Route::get('json/list', [
        'as' => "gallery.list", 'uses' => 'ArtistsController@jsonList'
      ]);
      Route::get('add', [
        'as' => "gallery.list", 'uses' => 'ArtistsController@addView'
      ]);
      Route::post('add', [
        'as' => "gallery.list", 'uses' => 'ArtistsController@store'
      ]);
      Route::get('edit/{id}', [
        'as' => "gallery.list", 'uses' => 'ArtistsController@editView'
      ]);
      Route::post('edit/{id}', [
        'as' => "gallery.list", 'uses' => 'ArtistsController@update'
      ]);
      Route::post('delete', [
        'as' => "gallery.list", 'uses' => 'ArtistsController@delete'
      ]);
    });
});
