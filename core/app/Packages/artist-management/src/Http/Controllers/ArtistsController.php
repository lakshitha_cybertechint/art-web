<?php
namespace ArtistManage\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Permissions\Models\Permission;
use ArtistManage\Models\Artist;
use Sentinel;
use Response;



class ArtistsController extends Controller {


	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');
	}

	/**
	 * Show the Branch add screen to the user.
	 *
	 * @return Response
	 */
	public function lists()
	{
		return view( 'artistManage::list');
	}

	/**
	 * Add new Branch data to database
	 *
	 * @return Redirect to Brach add
	 */
	public function store()
	{
		$validator =  $this->validate(request(), $this->getRules());
			if ($validator && $validator->fails()) {
				return redirect('admin/artists/add')->withErrors($validator->errors())->with([ 'error' => true,
				'error.message'=> 'Please check the errors',
				'error.title' => 'Oops!, Something went wrong']);;
			}else{
				$imageName =  $this->base64_img_upload(request()->image);
				$artist = Artist::create(request()->all());
				$artist->image = $imageName;
				$artist->save();

				if ($artist) {
					return redirect('admin/artists/list')->with([ 'success' => true,
						'success.message'=> 'Artist Created successfully!',
						'success.title' => 'Well Done!']);
				}
			}

	}

	/**
	 * View Branch List View
	 *
	 * @return Response
	 */
	public function addView()
	{
		return view( 'artistManage::add' );
	}

	/**
	 * Branch list
	 *
	 * @return Response
	 */
	public function jsonList(Request $request)
	{
		if($request->ajax()){
			$data= Artist::where('status', '!=', -1)->get();
			$jsonList = array();
			$i=1;
			foreach ($data as $key => $artist) {

				$dd = array();
				array_push($dd, $i);

				if($artist->name != ""){
					array_push($dd, $artist->name);
				}else{
					array_push($dd, "-");
				}
				if($artist->image != ""){
					array_push($dd, '<img src="'.url($artist->image).'" style="max-height : 100px; max-width : 200px;">');
				}else{
					array_push($dd, '<img src="'.url('assets/front/img/default_artist.png').'" style="max-height : 100px; max-width : 200px;">');
				}
				if($artist->birth_date != ""){
					array_push($dd, $artist->birth_date);
				}else{
					array_push($dd, "-");
				}
				if($artist->tagline != ""){
					array_push($dd, $artist->tagline);
				}else{
					array_push($dd, "-");
				}
				if($artist->created_at != ""){
					array_push($dd, $artist->created_at->toDateTimeString());
				}else{
					array_push($dd, "-");
				}

				$permissions = Permission::whereIn('name',['branch.edit','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\''.url('admin/artists/edit/'.$artist->id).'\'" data-toggle="tooltip" data-placement="top" title="Edit Artist"><i class="fa fa-pencil"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
				}

				$permissions = Permission::whereIn('name',['branch.delete','admin'])->where('status','=',1)->lists('name');
				if(Sentinel::hasAnyAccess($permissions)){
					array_push($dd, '<center><a class="artist-delete" data-id="'.$artist->id.'" data-toggle="tooltip" data-placement="top" title="Delete Artist"><i class="fa fa-trash-o"></i></a></center>');
				}else{
					array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
				}

				array_push($jsonList, $dd);
				$i++;
			}
			return Response::json(array('data'=>$jsonList));
		}else{
			return Response::json(array('data'=>[]));
		}
	}

public function editView($id)
{
	$artist = Artist::find($id);
	if ($artist) {
		return view('artistManage::edit')->with([
			'artist' => $artist
		]);
	}

	return redirect('admin/artists/list');
}

public function update($id)
{
	$artist = Artist::find($id);
	if ($artist) {
		$validator =  $this->validate(request(), $this->getRules($id));
		if ($validator && $validator->fails()) {
			return redirect('admin/artists/edit/'.$id)->withErrors($validator->errors())->with([ 'error' => true,
			'error.message'=> 'Please check the errors',
			'error.title' => 'Oops!, Something went wrong']);;
		}else{
			if (request()->image != "") {
				$imageName =  $this->base64_img_upload(request()->image);
				$artist->image = $imageName;
				$artist->save();
			}

			if ($artist->update(request()->except('image'))) {
				return redirect('admin/artists/list')->with([ 'success' => true,
					'success.message'=> 'Artist updated successfully!',
					'success.title' => 'Well Done!']);
			}
		}
	}

	return redirect('admin/artists/list');
}

public function delete()
{
	$artist = Artist::find(request()->id);
	if ($artist) {
		if ($artist->update([
				'status' => -1
			])) {
			return response()->json([
				"status" => "success"
			], 200);
		}

	}
	return response()->json(["unsuccess"], 500);
}

public function getRules($id = -1)
{
	$rules = [
		"name" => "required|unique:artists,name,".$id,
		"tagline" => "max:150",
		"birth_date" => "date",
	];

	// if ($id == -1) {
	// 	$rules["image"] = "required";
	// }

	return $rules;
}

public function uploadImage($file)
{
	$filePath = "core/storage/uploads/artists/";
	$fileName = "arts_".rand().".".$file->getClientOriginalExtension();
	$file->move($filePath, $fileName);

	return $filePath.$fileName;
}

public function base64_img_upload($img_data){//save base64 encoded image
	try {
		$newname = str_random(10).".png";
		$data = $img_data;
		list($type, $data) = explode(';', $data);
		list(, $data)      = explode(',', $data);
		$data = base64_decode($data);


		$save_target = "core/storage/uploads/artists//".$newname;

		$target = $save_target;


		if(file_put_contents($target, $data)){
			$img = imagecreatefrompng($target);
			imagealphablending($img, false);


			imagesavealpha($img, true);

			imagepng($img, $target, 8);
			return $save_target;
		}else{
			return "error";
		}
	} catch (Exception $e) {
		return "error";
	}



}
}
